import React, { Component } from 'react'
import { View } from 'react-native'

import Layout1 from './src/components/Layout1'
import Layout2 from './src/components/Layout2'

export default class App extends Component {
  render() {
    return (
      <View style={{ flex: 1 }}>
        {/* <Layout1 /> */}
        <Layout2 />
      </View>
    )
  }
}