import React, { Component } from 'react'
import { Text, View, StyleSheet } from 'react-native'

export default class Layout1 extends Component {
  render() {
    return (
      <View style={style.container}>
        <View style={[
          style.children,
          style.perfectCenter,
          { backgroundColor: '#fff' }
        ]}>
          <Text> Header </Text>
        </View>

        <View style={[style.children]} >
          <View style={[style.chirlden1, { backgroundColor: 'red' }]}>
            <View style={[style.chirlden2, { backgroundColor: '#ccc' }]}></View>
            <View style={[style.chirlden2, { backgroundColor: '#f96' }]}></View>
            <View style={[style.chirlden2, { backgroundColor: 'red' }]}></View>
          </View>
          <View style={[style.chirlden1]}>
            <View style={[style.chirlden2, { backgroundColor: '#f96' }]}></View>
            <View style={[style.chirlden2, { backgroundColor: 'red' }]}></View>
            <View style={[style.chirlden2, { backgroundColor: '#ccc' }]}></View>
          </View>
        </View>
      </View>
    )
  }
}


const style = StyleSheet.create({
  container: {
    flex: 1,
  },
  perfectCenter: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  children: {
    flex: 1,
    backgroundColor: 'green',
  },
  chirlden1: {
    flex: 1,
    flexDirection: 'row',
  },
  chirlden2: {
    flex: 1
  }
})