import React, { Component } from 'react'
import { View, StyleSheet, ScrollView } from 'react-native'

export default class Layout2 extends Component {
  render() {
    return (
      <View style={style.container}>
        <View style={style.headerContainer}>

        </View>

        <ScrollView
          showsVerticalScrollIndicator={false}
        >
          <View style={style.bodyContainer}>

            <View style={style.createPost}>
              <View style={style.mainCreate}>
                <ScrollView
                  horizontal
                  style={style.mainCreate}
                >
                  <View style={style.avatarContainer}>

                  </View>

                  <View style={style.textContainer}>

                  </View>
                  <View style={style.avatarContainer}>

                  </View>

                  <View style={style.textContainer}>

                  </View>
                  <View style={style.avatarContainer}>

                  </View>

                  <View style={style.textContainer}>

                  </View>
                  <View style={style.avatarContainer}>

                  </View>

                  <View style={style.textContainer}>

                  </View>
                </ScrollView>
              </View>

              <View style={style.footerCreate}>
              </View>
            </View>

            <PostThumb />
            <PostThumb />
            <PostThumb />
            <PostThumb />

          </View>
        </ScrollView>
      </View>
    )
  }
}

class PostThumb extends Component {
  render() {
    return (
      <View style={style.postView}>
        <View style={style.cardHeader}>

        </View>

        <View style={style.cardText}>

        </View>
        <View style={style.cardImage}>

        </View>
        <View style={style.cardStatus}>

        </View>

      </View>
    )
  }
}

const style = StyleSheet.create({
  container: {
    flex: 1,
  },
  headerContainer: {
    height: 75,
    backgroundColor: 'red',
  },

  bodyContainer: {
    flex: 1,
    // backgroundColor: '#e7e7e7',
    // paddingHorizontal: 5,
    marginHorizontal: 5
  },

  createPost: {
    height: 100,
    backgroundColor: 'orange',
    marginBottom: 5,
    borderWidth: 1,
    borderColor: '#e7e7e7',
    shadowColor: '#000',
    shadowOpacity: 0.1,
    shadowOffset: {
      width: 5,
      height: 5,
    }
  },

  mainCreate: {
    flex: 2,
    flexDirection: 'row'
  },

  avatarContainer: {
    // flex: 1,
    width: 70,
    backgroundColor: '#82d1dd',
  },

  textContainer: {
    width: 300,
    backgroundColor: '#ecf4dc',
  },

  footerCreate: {
    flex: 1,
    backgroundColor: '#ccdde1'
  },

  postView: {
    backgroundColor: '#aaa',
    marginVertical: 5,
    borderWidth: 1,
    borderColor: '#e7e7e7',
    shadowColor: '#000',
    shadowOpacity: 0.1,
    shadowOffset: {
      width: 5,
      height: 5,
    }
  },

  cardHeader: {
    backgroundColor: '#390071',
    height: 50,
  },

  cardText: {
    height: 100,
  },

  cardImage: {
    backgroundColor: '#82d1dd',
    height: 200,
  },

  cardStatus: {
    height: 50,
    backgroundColor: '#b5d77a'
  },
})